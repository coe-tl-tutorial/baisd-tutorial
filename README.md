# BAISD tutorial

for AI First Class

## The contributor is
* 5910130041 Thanathip Limna

### BAW
* 6410110507 Siwakorn Rotjanakarin
* 6410110475 Wanvisa Chaiuea
* 6410110187 Sapthawee Onchuenjit 

### GuJaJa
* 6410110064 Chakapat Suwannapong
* 6410110079 Jenitta Rattanakantha
* 6410110701 Phanuphong Lim

### DTY
* 6410110132 Don Angsupapong
* 6410110262 Nakin Sornpanya
* 6410110706 Marianee Saro-eng

### kaw_mun_kai
* 6410110724 Satit Deepeng
* 6410110287 Bunyawee Laongpun
* 6410110060 Jaturawich Khochun

### SOMETHING
* 6410110340 Pacharawut Thanawut
* 6410110194 Thanakrit Chimplipak
* 6410110301 Pathitta Suksomboon 

### room9
* 6410110625 Korawich Inthamusik
* 6410110559 Apinya Charoenlap
* 6410110635 Karif Topradu


### CFJ
* 6410110721 Sasitorn Sakkaew
* 6410110748 Arifin Mardsatun
* 6410110039 Kulwadee Suttajit


### BOP
* 6410110175 Nicha Vikromrotjananan
* 6410110626 Krittanai Chookliang
* 6410110738 Anuwat Khaokaew

### Big Boss
* 6410110337 Phongphiphat Khunchit
* 6410110326 Panisara Sangtong
* 6410110749 Idres Duereh
