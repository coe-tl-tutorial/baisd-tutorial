import random


class Fish:
    def __init__(self, name):
        self.name = name
        self.color = random.choice(["red", "green", "Blue", "Yellow", "Black", "Pink"])

    def jump(self):
        print(f"{self.name} is jumping")

    def __str__(self):
        return f"{self.name} is {self.color}"


class FancyCarp(Fish):
    def __init__(self, name):
        super().__init__(name)


if __name__ == "__main__":
    koiking = FancyCarp("KoiKing")

    print(koiking)
    koiking.jump()
